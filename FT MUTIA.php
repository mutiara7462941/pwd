<!DOCTYPE html>
<!-- saved from url=(0038)https://web.telegram.org/k/#6086485359 -->
<html lang="en" class="overlay-scroll no-touch" style="--vh:9.36px; --primary-color:#3390ec; --primary-color-rgb:51,144,236; --light-primary-color:rgba(51, 144, 236, 0.08); --light-filled-primary-color:#eef6fd; --dark-primary-color:hsl(209.838, 82.9596%, 52.2745%); --saved-color:#3390ec; --light-filled-saved-color:#7cb7f2; --message-out-background-color:#e9f5e9; --light-message-out-background-color:rgba(233, 245, 233, 0.12); --light-filled-message-out-background-color:#fcfdfc; --dark-message-out-background-color:hsl(120, 37.5%, 81.7255%); --message-out-primary-color:#4fae4e; --light-filled-message-out-primary-color:#dcefdc; --surface-color:#ffffff; --surface-color-rgb:255,255,255; --danger-color:#df3f40; --light-danger-color:rgba(223, 63, 64, 0.08); --dark-danger-color:hsl(359.625, 71.4286%, 48.0784%); --primary-text-color:#000000; --primary-text-color-rgb:0,0,0; --secondary-text-color:#707579; --light-secondary-text-color:rgba(112, 117, 121, 0.08); --light-filled-secondary-text-color:#f3f3f4; --message-background-color:#ffffff; --light-message-background-color:rgba(255, 255, 255, 0.08); --light-filled-message-background-color:#ffffff; --dark-message-background-color:hsl(0, 0%, 92%); --green-color:#70b768; --message-highlightning-color:hsla(86.4, 43.8462%, 45.1176%, 0.4); --messages-text-size:16px; --left-column-width:420px; --right-column-proportion:0.333333;" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>Telegram Web</title>
    <meta name="description" content="Telegram is a cloud-based mobile and desktop messaging app with a focus on security and speed.">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no,shrink-to-fit=no,viewport-fit=cover"> 
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-title" content="Telegram Web">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Telegram Web">
    <meta name="application-name" content="Telegram Web">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-TileImage" content="assets/img/mstile-144x144.png?v=jw3mK7G9Ry">
    <meta name="msapplication-config" content="browserconfig.xml?v=jw3mK7G9Ry">
    <meta name="theme-color" content="#79a541">
    <meta name="color-scheme" content="light">
    <meta name="google" content="notranslate">
    <!-- <meta name="robots" content="noindex"> -->

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://web.telegram.org/k/">
    <meta property="og:title" content="Telegram Web">
    <meta property="og:description" content="Telegram is a cloud-based mobile and desktop messaging app with a focus on security and speed.">
    <meta property="og:image" content="assets/img/android-chrome-192x192.png?v=jw3mK7G9Ry">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://web.telegram.org/k/">
    <meta property="twitter:title" content="Telegram Web">
    <meta property="twitter:description" content="Telegram is a cloud-based mobile and desktop messaging app with a focus on security and speed.">
    <meta property="twitter:image" content="assets/img/android-chrome-192x192.png?v=jw3mK7G9Ry">

    <link rel="apple-touch-icon" sizes="180x180" href="https://web.telegram.org/k/assets/img/apple-touch-icon.png?v=jw3mK7G9Ry">
    <link rel="icon" type="image/png" sizes="16x16" href="https://web.telegram.org/k/assets/img/favicon-16x16.png?v=jw3mK7G9Ry">
    <link rel="icon" type="image/png" sizes="32x32" href="https://web.telegram.org/k/assets/img/favicon-32x32.png?v=jw3mK7G9Ry">
    <link rel="icon" type="image/png" sizes="192x192" href="https://web.telegram.org/k/assets/img/android-chrome-192x192.png?v=jw3mK7G9Ry">
    <link rel="alternate icon" href="https://web.telegram.org/k/assets/img/favicon.ico?v=jw3mK7G9Ry" type="image/x-icon">
    <!-- <link rel="mask-icon" href="assets/img/logo_filled.svg?v=jw3mK7G9Ry" color="#3390ec"> -->
    <link rel="canonical" href="https://web.telegram.org/">

    <link rel="manifest" id="manifest" href="https://web.telegram.org/k/site.webmanifest?v=jw3mK7G9Aq">
    <script type="module" crossorigin="" src="./FT MUTIA_files/index-257913ec.js.download"></script>
    <link rel="stylesheet" href="./FT MUTIA_files/index-70fb3a96.css">
  <style>.night {--primary-color:#8774e1; --primary-color-rgb:135,116,225; --light-primary-color:rgba(135, 116, 225, 0.08); --light-filled-primary-color:#292730; --dark-primary-color:hsl(250.459, 64.497%, 62.8627%); --saved-color:#8774e1; --light-filled-saved-color:#b2a6eb; --message-out-background-color:#7e6dd1; --light-message-out-background-color:rgba(126, 109, 209, 0.92); --light-filled-message-out-background-color:#7666c2; --dark-message-out-background-color:hsl(250.2, 52.0833%, -29.6471%); --message-out-primary-color:#ffffff; --light-filled-message-out-primary-color:#8878d4; --surface-color:#212121; --surface-color-rgb:33,33,33; --danger-color:#ff595a; --light-danger-color:rgba(255, 89, 90, 0.08); --dark-danger-color:hsl(359.639, 100%, 59.451%); --primary-text-color:#ffffff; --primary-text-color-rgb:255,255,255; --secondary-text-color:#aaaaaa; --light-secondary-text-color:rgba(170, 170, 170, 0.08); --light-filled-secondary-text-color:#2b2b2b; --message-background-color:#212121; --light-message-background-color:rgba(33, 33, 33, 0.08); --light-filled-message-background-color:#212121; --dark-message-background-color:hsl(0, 0%, 4.94118%); --green-color:#5CC85E;}</style><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/pageIm-bd2fe9a7.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/page-e73ef7e4.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/appDialogsManager-d08043ec.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/avatar-74c056ee.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/button-a9a2d121.js"><link rel="modulepreload" as="script" crossorigin="" href="./FT MUTIA_files/index-257913ec.js.download"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/wrapEmojiText-a69151ca.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/scrollable-93cbead8.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/putPreloader-339e0b30.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/htmlToSpan-d5fc6142.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/countryInputField-566898ce.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/textToSvgURL-c6ebb454.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/toggleDisability-6f5940d7.js"><link rel="modulepreload" as="script" crossorigin="" href="https://web.telegram.org/k/codeInputField-53581107.js"><link rel="stylesheet" href="./FT MUTIA_files/appDialogsManager-9e4e8073.css"></head>
  <body class="animation-level-2 has-chat">
    <!--[if IE]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
    <svg style="position: absolute; top: -10000px; left: -10000px;">
      <defs id="svg-defs">
        <clippath id="tooltip-notch-clip">
          <path d="M19 7C16.8992 7 13.59 3.88897 11.5003 1.67424C10.7648 0.894688 10.397 0.50491 10.0434 0.385149C9.70568 0.270811 9.4225 0.270474 9.08456 0.38401C8.73059 0.50293 8.36133 0.892443 7.62279 1.67147C5.52303 3.88637 2.18302 7 0 7L19 7Z"></path>
        </clippath>
        <clippath id="message-tail-clip">
          <use href="#message-tail"></use>
        </clippath>
        <!-- <linearGradient id="topic-icon-gradient" x1="0" x2="0" y1="0" y2="1">
          <stop class="topic-icon-gradient-stop-1" offset="0%" />
          <stop class="topic-icon-gradient-stop-2" offset="100%" />
        </linearGradient> -->
        <path id="message-tail" d="M1.00002881,1.03679295e-14 L7,0 L7,17 C6.8069969,14.1607017 6.12380234,11.2332513 4.95041634,8.21764872 C4.04604748,5.89342034 2.50413132,3.73337411 0.324667862,1.73751004 L0.324652538,1.73752677 C-0.0826597201,1.36452676 -0.110475289,0.731958677 0.262524727,0.324646419 C0.451952959,0.117792698 0.719544377,1.0985861e-14 1.00002881,1.04360964e-14 Z"></path>
        <path id="logo" fill-rule="evenodd" d="M80,0 C124.18278,0 160,35.81722 160,80 C160,124.18278 124.18278,160 80,160 C35.81722,160 0,124.18278 0,80 C0,35.81722 35.81722,0 80,0 Z M114.262551,46.4516129 L114.123923,46.4516129 C111.089589,46.5056249 106.482806,48.0771432 85.1289541,56.93769 L81.4133571,58.4849956 C72.8664779,62.0684477 57.2607933,68.7965125 34.5963033,78.66919 C30.6591745,80.2345564 28.5967328,81.765936 28.4089783,83.2633288 C28.0626453,86.0254269 31.8703852,86.959903 36.7890378,88.5302703 L38.2642674,89.0045258 C42.3926354,90.314406 47.5534685,91.7248852 50.3250916,91.7847532 C52.9151948,91.8407003 55.7944784,90.8162976 58.9629426,88.7115451 L70.5121776,80.9327422 C85.6657026,70.7535853 93.6285785,65.5352892 94.4008055,65.277854 L94.6777873,65.216416 C95.1594319,65.1213105 95.7366278,65.0717596 96.1481181,65.4374337 C96.6344248,65.8695939 96.5866185,66.6880224 96.5351057,66.9075859 C96.127514,68.6448691 75.2839361,87.6143392 73.6629144,89.2417998 L73.312196,89.6016896 C68.7645143,94.2254793 63.9030972,97.1721503 71.5637945,102.355193 L73.3593638,103.544598 C79.0660342,107.334968 82.9483395,110.083813 88.8107882,113.958377 L90.3875424,114.996094 C95.0654739,118.061953 98.7330313,121.697601 103.562866,121.253237 C105.740839,121.052855 107.989107,119.042224 109.175465,113.09692 L109.246762,112.727987 C112.002037,98.0012935 117.417883,66.09303 118.669527,52.9443975 C118.779187,51.7924073 118.641237,50.318088 118.530455,49.6708963 L118.474159,49.3781963 C118.341081,48.7651315 118.067967,48.0040758 117.346762,47.4189793 C116.412565,46.6610871 115.002114,46.4638844 114.262551,46.4516129 Z"></path>
        <path id="poll-line" d="M4.47,5.33v13.6c0,4.97,4.03,9,9,9h458.16"></path>
        <path id="check" fill="none" d="M 4.7071 12.2929 l 5 5 c 0.3905 0.3905 1.0237 0.3905 1.4142 0 l 11 -11"></path>
        <path id="verified-icon-background" fill-rule="evenodd" clip-rule="evenodd" d="m14.378741 1.509638 1.818245 1.818557c.365651.365716.861601.571194 1.378741.571259l2.574273.000312c1.01361.000117 1.846494.773578 1.940861 1.762436l.008905.187798-.000312 2.5727c-.000065.517322.205439 1.013454.571259 1.379222l1.819649 1.819337c.714441.713427.759174 1.843179.134563 2.609139l-.134797.148109-1.819181 1.8182502c-.365963.3657823-.571558.8620196-.571493 1.3794456l.000312 2.5737972c.000559 1.0136048-.772668 1.846676-1.7615 1.9412861l-.188266.0084786-2.573792-.0003107c-.517426-.0000624-1.013675.2055248-1.379456.5714956l-1.818245 1.8191823c-.71331.7145515-1.843049.7594886-2.609113.1349998l-.148135-.1347645-1.8193435-1.8196542c-.3657628-.3658252-.8618987-.5713214-1.3792103-.571259l-2.5727052.0003107c-1.0136048.0001222-1.846676-.7731321-1.9412861-1.761968l-.0089492-.1877967-.0003107-2.5742678c-.0000624-.5171478-.2055495-1.0130926-.571259-1.3787397l-1.8185622-1.8182515c-.7139886-.713869-.758706-1.843647-.1340846-2.609607l.1338493-.148109 1.8190328-1.81935c.3655665-.365625.5709613-.861471.5710237-1.378494l.0003107-2.573181c.0006006-1.076777.8734635-1.949636 1.9502353-1.950234l2.5731758-.000312c.5170321-.000065 1.0128768-.205452 1.3785044-.571025l1.8193448-1.819038c.761592-.761449 1.996254-.761345 2.757716.000247zm3.195309 8.047806c-.426556-.34125-1.032655-.306293-1.417455.060333l-.099151.108173-4.448444 5.55815-1.7460313-1.74707-.1104961-.096564c-.4229264-.32188-1.0291801-.289692-1.4154413.096564-.3862612.386269-.4184492.992511-.0965653 1.41544l.0965653.1105 2.5999987 2.5999987.109876.0961467c.419874.320359 1.015131.2873897 1.397071-.0773773l.098579-.107692 5.2-6.4999961.083772-.120484c.273208-.455884.174278-1.054885-.252278-1.396122z"></path>
        <path id="verified-icon-check" d="M8 8H18V18H8V8Z"></path>
        <path id="topic-icon" d="M21.3733 4.27035C19.2176 2.25323 16.2016 1 12.8636 1C6.31153 1 1 5.82866 1 11.7851C1 15.1823 2.58685 17.9669 5.28707 19.9438C5.63238 20.1966 5.95042 21.6013 5.18073 22.7812C4.86598 23.2637 4.51948 23.639 4.25272 23.928C3.86715 24.3457 3.6482 24.5828 3.93266 24.7022C4.22935 24.8266 5.98245 24.8882 7.24776 24.1786C8.23812 23.6232 8.83793 23.0691 9.24187 22.696C9.57588 22.3875 9.77595 22.2027 9.95217 22.2431C10.584 22.388 11.236 22.4868 11.9035 22.5354C11.9126 22.5361 11.9218 22.5368 11.9309 22.5374C12.2386 22.5592 12.5497 22.5702 12.8636 22.5702C19.4157 22.5702 24.7273 17.7416 24.7273 11.7851C24.7273 8.86318 23.4491 6.21263 21.3733 4.27035Z"></path>
        <!-- <path id="verified-icon-background" d="M12.3 2.9c.1.1.2.1.3.2.7.6 1.3 1.1 2 1.7.3.2.6.4.9.4.9.1 1.7.2 2.6.2.5 0 .6.1.7.7.1.9.1 1.8.2 2.6 0 .4.2.7.4 1 .6.7 1.1 1.3 1.7 2 .3.4.3.5 0 .8-.5.6-1.1 1.3-1.6 1.9-.3.3-.5.7-.5 1.2-.1.8-.2 1.7-.2 2.5 0 .4-.2.5-.6.6-.8 0-1.6.1-2.5.2-.5 0-1 .2-1.4.5-.6.5-1.3 1.1-1.9 1.6-.3.3-.5.3-.8 0-.7-.6-1.4-1.2-2-1.8-.3-.2-.6-.4-.9-.4-.9-.1-1.8-.2-2.7-.2-.4 0-.5-.2-.6-.5 0-.9-.1-1.7-.2-2.6 0-.4-.2-.8-.4-1.1-.6-.6-1.1-1.3-1.6-2-.4-.4-.3-.5 0-1 .6-.6 1.1-1.3 1.7-1.9.3-.3.4-.6.4-1 0-.8.1-1.6.2-2.5 0-.5.1-.6.6-.6.9-.1 1.7-.1 2.6-.2.4 0 .7-.2 1-.4.7-.6 1.4-1.2 2.1-1.7.1-.2.3-.3.5-.2z" /> -->
        <!-- <path id="verified-icon-check" d="M16.4 10.1l-.2.2-5.4 5.4c-.1.1-.2.2-.4 0l-2.6-2.6c-.2-.2-.1-.3 0-.4.2-.2.5-.6.7-.6.3 0 .5.4.7.6l1.1 1.1c.2.2.3.2.5 0l4.3-4.3c.2-.2.4-.3.6 0 .1.2.3.3.4.5.2 0 .3.1.3.1z" /> -->
        <symbol id="message-tail-filled" viewBox="0 0 11 20">
          <g transform="translate(9 -14)" fill="inherit" fill-rule="evenodd">
            <path d="M-6 16h6v17c-.193-2.84-.876-5.767-2.05-8.782-.904-2.325-2.446-4.485-4.625-6.48A1 1 0 01-6 16z" transform="matrix(1 0 0 -1 0 49)" id="corner-fill" fill="inherit"></path>
          </g>
        </symbol>
        <!-- <linearGradient id="g" x1="-300%" x2="-200%" y1="0" y2="0">
          <stop offset="-10%" stop-opacity=".1"/>
          <stop offset="30%" stop-opacity=".07"/>
          <stop offset="70%" stop-opacity=".07"/>
          <stop offset="110%" stop-opacity=".1"/>
          <animate attributeName="x1" from="-300%" to="1200%" dur="3s" repeatCount="indefinite"/>
          <animate attributeName="x2" from="-200%" to="1300%" dur="3s" repeatCount="indefinite"/>
        </linearGradient> -->
        <!-- <linearGradient id="stories-gradient" x1="91.56%" y1="-0.56%" x2="13.42%" y2="102.37%">
          <stop offset="0%" stop-color="#34C76F"></stop>
          <stop offset="100%" stop-color="#3DA1FD"></stop>
        </linearGradient> -->
      </defs>
    </svg>
    
    <div class="whole page-chats" style="" id="page-chats">
      <div id="main-columns" class="tabs-container" data-animation="navigation" style="">
        <div class="tabs-tab chatlist-container sidebar sidebar-left main-column" id="column-left">
          <div class="sidebar-slider tabs-container" data-animation="navigation">
            <div class="tabs-tab sidebar-slider-item item-main active">
              <div class="sidebar-header can-have-forum">
                <div class="sidebar-header__btn-container">
                  <div class="animated-menu-icon"></div>
                  <button class="btn-icon rp btn-menu-toggle sidebar-tools-button is-visible"><div class="c-ripple"></div></button><div class="btn-icon sidebar-back-button"></div>
                </div>
              <div class="input-search">
      <input type="text" autocomplete="off" class="input-field-input input-search-input is-empty" dir="auto" placeholder=" ">
      <div class="input-field-border"></div><span class="tgico input-search-icon input-search-part will-animate"></span><button class="btn-icon input-search-clear input-search-part rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><span class="i18n input-search-placeholder will-animate">Search</span></div></div><div class="stories-list"><div class="_ListContainer_6d8jp_105 disable-hover _skipAnimation_6d8jp_113" style="transform: translateY(-69px); --progress:1;"><div class="scrollable scrollable-x"><div class="_List_6d8jp_31 _space-evenly_6d8jp_35"></div></div></div></div>
              <div class="sidebar-content transition zoom-fade can-have-forum" data-animation="zoom-fade">
                <div class="transition-item active" id="chatlist-container" data-transition-timeout="23" style="--stories-scrolled:92px;">
                  
                  
                <div class="connection-status-bottom"><div class="folders-tabs-scrollable menu-horizontal-scrollable hide">
                    
                  <div class="scrollable scrollable-x"><nav class="menu-horizontal-div" id="folders-tabs"><div class="menu-horizontal-div-item rp active" data-filter-id="0"><div class="c-ripple"></div><span class="menu-horizontal-div-item-span"><span class="text-super"><span class="i18n">All Chats</span></span><div class="badge badge-20 badge-primary">6</div><i></i></span></div></nav></div></div><div class="tabs-container" id="folders-container" data-animation="tabs"><div class="scrollable scrollable-y tabs-tab chatlist-parts active" data-filter-id="0" style="overflow-y: hidden;"><div class="chatlist-top"><ul class="chatlist" data-autonomous="0"><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big active is-muted" href="https://web.telegram.org/k/#6086485359" data-peer-id="6086485359"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto">Ngpi ini</div><div class="dialog-subtitle-badge badge badge-22 is-visible badge-icon dialog-pinned-icon"><span class="tgico"></span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="6086485359" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Sayang<img src="./FT MUTIA_files/1f412.png" class="emoji emoji-image" alt="🐒"><img src="./FT MUTIA_files/1f90d.png" class="emoji emoji-image" alt="🤍"></span><span class="tgico dialog-muted-icon"></span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">12:12 PM</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="6086485359" data-color="green">S</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-1799182735" data-peer-id="-1799182735"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="5304477884" data-only-first-name="1">zhr</span>: </span>baik pak terimakasih</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-1799182735" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Pemrograman Web Dasar (02011426) - Kamis (11:30 - 14:50)</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">11:49 AM</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-1799182735" data-color="red">P1</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5262617141" data-peer-id="5262617141"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span>Pemrograman Web Dasar Materi full.pptx</span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5262617141" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0"><span class="i18n">Saved Messages</span></span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Wed</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5262617141"><span class="tgico avatar-icon avatar-icon-saved"></span></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4086020527" data-peer-id="-4086020527"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="180722661" data-only-first-name="1">Dini</span>: </span><div class="dialog-subtitle-media media-container"><canvas class="canvas-thumbnail thumbnail media-photo" width="40" height="34"></canvas></div>contoh penyelesaian</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4086020527" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Basis Data</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Wed</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4086020527" data-color="orange">BD</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1035019828" data-peer-id="1035019828"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><div class="dialog-subtitle-media media-container"><canvas class="canvas-thumbnail thumbnail media-photo" width="40" height="6"></canvas></div><span><span class="i18n">Photo</span></span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1035019828" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">amelia putri</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Wed</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger avatar-relative" data-peer-id="1035019828" data-color="green"><img class="avatar-photo avatar-photo-thumbnail" decoding="async" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDACgcHiMeGSgjISMtKygwPGRBPDc3PHtYXUlkkYCZlo+AjIqgtObDoKrarYqMyP/L2u71////m8H////6/+b9//j/2wBDASstLTw1PHZBQXb4pYyl+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj/wAARCAAIAAgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwB7W7bAdx3HoKKKKmxdj//Z">ap</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4061319431" data-peer-id="-4061319431"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="1236468798" data-only-first-name="1">Mitha Ayu Wandari</span>: </span><div class="dialog-subtitle-media media-container"><canvas class="canvas-thumbnail thumbnail media-photo" width="23" height="40"></canvas></div><span><span class="i18n">Photo</span></span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4061319431" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Perencanaan Dan Pengembangan Produk | Rabu (08:00 - 11:20)</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Wed</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4061319431" data-color="orange">P1</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4094324558" data-peer-id="-4094324558"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="284827075" data-only-first-name="1">mutiara lusiana</span>: </span>14.50 kita closing ujian kuis nya ya adek adek</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4094324558" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Statistika dan Probabilitas (11.30-14.50)</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4094324558" data-color="violet">S(</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-1620414488" data-peer-id="-1620414488"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="232467475" data-only-first-name="1">Refael</span>: </span>https://palcomtech.ac.id/club-fotografi-palcomtech/</div><div class="dialog-subtitle-badge badge badge-22 is-visible unread">1</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-1620414488" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Official PalComTech 2022</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-1620414488"><img class="avatar-photo" src="blob:https://web.telegram.org/3c760369-a18d-4199-8d63-ad35b1331d23"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#284827075" data-peer-id="284827075"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto">ok</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="284827075" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">mutiara lusiana annisa</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="284827075" data-color="orange">ma</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1452485203" data-peer-id="1452485203"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto">Dik kan kau ketuo kelas MK  pemrograman web dasar pak Andika minta ling materi nyo</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1452485203" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Diki India Palcomtech</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"><span class="tgico sending-status-icon sending-status-icon-checks"></span></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger is-online" data-peer-id="1452485203" data-color="orange">DP</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-1887848208" data-peer-id="-1887848208"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="5477033722" data-only-first-name="1">Informasi Terkini Institut PalComTech</span>: </span>Hai.. Hai.. Hai.. 

Kamu suka desain ? Atau pemula yang ingin belajar desain?
Eitsssssss… PASSS BANG..<img src="https://web.telegram.org/k/assets/img/emoji/203c.png" class="emoji emoji-image" alt="."></div><div class="dialog-subtitle-badge badge badge-22 is-visible unread">3</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-1887848208" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">PA Febria Angkatan 22-23-24-25</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-1887848208" data-color="orange">P2</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4001969025" data-peer-id="-4001969025"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="6025163214" data-only-first-name="1">Fadillah</span>: </span>{*_*}</div><div class="dialog-subtitle-badge badge badge-22 is-visible unread">6</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4001969025" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Mam Ria - Reading</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Tue</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4001969025"><img class="avatar-photo" src="blob:https://web.telegram.org/9de2ee41-7696-46a7-99fe-4dc05afa21bd"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1175378594" data-peer-id="1175378594"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span><span class="i18n">Voice message</span></span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1175378594" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Ria Octarina</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"><span class="tgico sending-status-icon sending-status-icon-check"></span></span><span class="message-time"><span class="i18n" dir="auto">Mon</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1175378594" data-color="blue">RO</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5089163883" data-peer-id="5089163883"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto">iya</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5089163883" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Eka Hartati</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Mon</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5089163883" data-color="pink">EH</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4048638917" data-peer-id="-4048638917"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="5089163883" data-only-first-name="1">Eka</span>: </span>kita sudah selesai bljr nya dek</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4048638917" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">MK Etika Profesi</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Mon</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4048638917" data-color="orange">MP</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#-4051667641" data-peer-id="-4051667641"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="primary-text"><span class="peer-title" dir="auto" data-peer-id="1511224727" data-only-first-name="1">Putri Aulia</span>: </span>Fero kato kau nk nanyo minggu ini</div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="-4051667641" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Kelompok 8 Etika Profesi</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status hide"></span><span class="message-time"><span class="i18n" dir="auto">Mon</span></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="-4051667641" data-color="red">KP</div></a></ul></div><div class="chatlist-bottom"></div></div></div></div></div>
                <div class="transition-item sidebar-search" id="search-container" data-transition-timeout="88"><div class="search-super-tabs-scrollable menu-horizontal-scrollable sticky"><div class="scrollable scrollable-x search-super-nav-scrollable"><nav class="search-super-tabs menu-horizontal-div"><div class="menu-horizontal-div-item rp active"><span class="menu-horizontal-div-item-span"><span class="i18n">Chats</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Media</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Links</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Files</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Music</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Voice</span><i></i></span><div class="c-ripple"></div></div></nav><div class="search-helper hide"></div></div></div><div class="scrollable scrollable-y"><div class="search-super"><div class="search-super-tabs-container tabs-container" data-animation="tabs"><div class="search-super-tab-container search-super-container-chats tabs-tab active"><div class="search-super-content-container search-super-content-chats"><div class="search-group search-group-contacts" style="display: none;"><div class="search-group__name"><span class="i18n">Chats</span></div><ul class="chatlist" data-autonomous="1"></ul></div><div class="search-group search-group-contacts is-short" style="display: none;"><div class="search-group__name"><span class="i18n">Global search</span></div><ul class="chatlist" data-autonomous="1"></ul></div><div class="search-group search-group-messages" style="display: none;"><div class="search-group__name"><span class="i18n">Messages</span></div><ul class="chatlist" data-autonomous="1"></ul></div><div class="search-group-people search-group search-group-contacts" style=""><div class="search-group-scrollable"><div class="scrollable scrollable-x"><ul class="chatlist" data-autonomous="0"><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1185663819" data-peer-id="1185663819"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1185663819" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Ipin</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1185663819" data-color="cyan">I</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1175378594" data-peer-id="1175378594"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1175378594" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Ria</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1175378594" data-color="blue">RO</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big active" href="https://web.telegram.org/k/#6086485359" data-peer-id="6086485359"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="6086485359" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Sayang<img src="./FT MUTIA_files/1f412.png" class="emoji emoji-image" alt="🐒"><img src="./FT MUTIA_files/1f90d.png" class="emoji emoji-image" alt="🤍"></span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="6086485359" data-color="green">S</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5957000804" data-peer-id="5957000804"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5957000804" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">~zeyy~</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5957000804" data-color="pink">~</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1322695942" data-peer-id="1322695942"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1322695942" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Rahma</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1322695942"><img class="avatar-photo" src="blob:https://web.telegram.org/704e3f03-6b57-42ab-8590-3b8df760d00a"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1452485203" data-peer-id="1452485203"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1452485203" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Diki India</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1452485203" data-color="orange">DP</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5089163883" data-peer-id="5089163883"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5089163883" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Eka</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5089163883" data-color="pink">EH</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5385305776" data-peer-id="5385305776"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5385305776" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Maharani Si</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5385305776"><img class="avatar-photo" src="blob:https://web.telegram.org/c44b5c4b-c459-4b17-b5ad-099f8a4bace8"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#284827075" data-peer-id="284827075"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="284827075" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">mutiara lusiana</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="284827075" data-color="orange">ma</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5082501375" data-peer-id="5082501375"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5082501375" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Mutiara Lusiana</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5082501375" data-color="red">MA</div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1148699474" data-peer-id="1148699474"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1148699474" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">M.Ilham</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1148699474"><img class="avatar-photo" src="blob:https://web.telegram.org/f4801d24-1a1b-4bac-a279-0f5d530c9f2b"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5540233587" data-peer-id="5540233587"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5540233587" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Feronika</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5540233587"><img class="avatar-photo" src="blob:https://web.telegram.org/eaa6760b-c0ba-4755-95c1-9a96f4393203"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#5440899031" data-peer-id="5440899031"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="5440899031" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Riky Si</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="5440899031"><img class="avatar-photo" src="blob:https://web.telegram.org/d524c2b4-37eb-4a68-ad10-2a5367c4d373"></div></a><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-bigger row-big" href="https://web.telegram.org/k/#1487531406" data-peer-id="1487531406"><div class="c-ripple"></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="1487531406" data-from-name="0" data-dialog="1" data-only-first-name="1" data-with-icons="0" data-thread-id="0">Rachmad Kang Bestie</span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-54 dialog-avatar row-media row-media-bigger" data-peer-id="1487531406"><img class="avatar-photo" src="blob:https://web.telegram.org/183d87be-5703-4fa0-b93d-d73a884a30a1"></div></a></ul></div></div></div><div class="search-group-recent search-group search-group-contacts" style=""><div class="search-group__name"><span class="i18n">Recent</span><button class="btn-icon rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button></div><ul class="chatlist" data-autonomous="1"><a class="row no-wrap row-with-padding row-clickable hover-effect rp chatlist-chat chatlist-chat-abitbigger" data-peer-id="6086485359"><div class="c-ripple"></div><div class="row-row row-subtitle-row dialog-subtitle"><div class="row-subtitle no-wrap" dir="auto"><span class="i18n">last seen recently</span></div></div><div class="row-row row-title-row dialog-title"><div class="row-title no-wrap user-title" dir="auto"><span class="peer-title" dir="auto" data-peer-id="6086485359" data-from-name="0" data-dialog="1" data-only-first-name="0" data-with-icons="1" data-thread-id="0">Sayang<img src="./FT MUTIA_files/1f412.png" class="emoji emoji-image" alt="🐒"><img src="./FT MUTIA_files/1f90d.png" class="emoji emoji-image" alt="🤍"></span></div><div class="row-title row-title-right row-title-right-secondary dialog-title-details"><span class="message-status sending-status"></span><span class="message-time"></span></div></div><div class="avatar avatar-like avatar-42 dialog-avatar row-media row-media-abitbigger" data-peer-id="6086485359" data-color="green">S</div></a></ul></div></div></div><div class="search-super-tab-container search-super-container-media tabs-tab"><div class="search-super-content-container search-super-content-media"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-links tabs-tab"><div class="search-super-content-container search-super-content-links"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-files tabs-tab"><div class="search-super-content-container search-super-content-files"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-music tabs-tab"><div class="search-super-content-container search-super-content-music"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-voice tabs-tab"><div class="search-super-content-container search-super-content-voice"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div></div></div></div></div>
              <button class="btn-circle rp btn-corner z-depth-1 btn-menu-toggle animated-button-icon" tabindex="-1" id="new-menu"><span class="tgico animated-button-icon-icon animated-button-icon-icon-first"></span><span class="tgico animated-button-icon-icon animated-button-icon-icon-last"></span><div class="c-ripple"></div></button><div class="btn-circle rp btn-corner z-depth-1 btn-update is-hidden" tabindex="-1"><div class="c-ripple"></div><span class="i18n">UPDATE</span></div></div>
            <div class="topics-slider"></div></div>
          </div>
        </div>
        <div class="tabs-tab main-column" id="column-center"><div class="chats-container tabs-container" data-animation="navigation"><div class="chat tabs-tab active" data-type="chat"><div class="chat-background"><div class="chat-background-item is-pattern is-visible"><canvas width="50" height="50" data-colors="#dbddbb,#6ba587,#d5d88d,#88b884" class="chat-background-item-canvas chat-background-item-color-canvas"></canvas><canvas data-original-height="935.984375" width="1260" height="1403" class="chat-background-item-canvas chat-background-item-pattern-canvas" style="--opacity-max:0.5;"></canvas></div></div><div class="sidebar-header topbar" data-floating="0" style="--utils-width:144px;"><div class="chat-info-container"><button class="btn-icon sidebar-close-button"><span class="tgico button-icon"></span><span class="badge badge-20 badge-primary back-unread-badge">6</span></button><div class="chat-info"><div class="person"><div class="avatar avatar-like avatar-42 person-avatar" data-peer-id="6086485359" data-color="green">S</div><div class="content"><div class="top"><div class="user-title"><span class="peer-title" dir="auto" data-peer-id="6086485359" data-dialog="1" data-with-icons="1" data-thread-id="0">Sayang<img src="./FT MUTIA_files/1f412.png" class="emoji emoji-image" alt="🐒"><img src="./FT MUTIA_files/1f90d.png" class="emoji emoji-image" alt="🤍"></span></div></div><div class="bottom"><div class="info"><span class="i18n">last seen recently</span></div></div></div></div></div><div class="chat-utils"><div class="pinned-message pinned-container hide"><button class="btn-icon pinned-container-close pinned-message-close"><span class="tgico button-icon"></span></button><div class="pinned-container-wrapper pinned-message-wrapper rp"><div class="c-ripple"></div><div class="pinned-message-border"><div class="pinned-message-border-wrapper-1" style=""></div></div><div class="pinned-message-content pinned-container-content"><div class="animated-super pinned-message-media-container"></div><div class="pinned-message-title pinned-container-title" dir="auto"><span class="i18n">Pinned Message</span> <div class="animated-counter"></div></div><div class="pinned-message-subtitle pinned-container-subtitle" dir="auto"><div class="animated-super"></div></div></div><div class="pinned-container-wrapper-utils pinned-message-wrapper-utils"><button class="btn-icon pinned-container-close pinned-message-pinlist"><span class="tgico button-icon"></span></button><button class="btn-icon pinned-container-close pinned-message-close"><span class="tgico button-icon"></span></button></div></div></div><button class="btn-primary btn-color-primary chat-join hide rp"><div class="c-ripple"></div></button><button class="btn-icon chat-pinlist rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><button class="btn-icon rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><button class="btn-icon rp hide"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><button class="btn-icon rp hide" style="display: none;"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><button class="btn-icon rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><button class="btn-icon rp btn-menu-toggle"><div class="c-ripple"></div><span class="tgico button-icon"></span></button></div></div><div class="pinned-audio pinned-container hide"><div class="pinned-container-wrapper pinned-audio-wrapper rp"><div class="c-ripple"></div><button class="btn-icon active"><span class="tgico button-icon"></span></button><button class="btn-icon active pinned-audio-ico"></button><button class="btn-icon active"><span class="tgico button-icon"></span></button><div class="pinned-audio-content pinned-container-content"><div class="pinned-audio-title pinned-container-title" dir="auto"></div><div class="pinned-audio-subtitle pinned-container-subtitle" dir="auto"></div></div><div class="pinned-audio-progress-wrapper"><div class="progress-line use-transform pinned-audio-progress"><div class="progress-line__filled"></div><input class="progress-line__seek" type="range" step="0.016666666666666666" min="0" max="1"></div></div><div class="pinned-container-wrapper-utils pinned-audio-wrapper-utils"><div class="btn-icon player-volume pinned-audio-volume active"><div class="pinned-audio-volume-tunnel"></div><span class="tgico button-icon player-volume__icon"></span><div class="progress-line-container"><div class="progress-line"><div class="progress-line__filled" style="width: 100%;"></div><input class="progress-line__seek" type="range" step="0.01" min="0" max="1"></div></div></div><button class="btn-icon"><span class="tgico button-icon"></span></button><button class="btn-icon"><span class="tgico button-icon"></span></button><button class="btn-icon pinned-container-close pinned-audio-close"><span class="tgico button-icon"></span></button></div></div></div><div class="pinned-requests pinned-container hide"><div class="pinned-container-wrapper pinned-requests-wrapper rp"><div class="c-ripple"></div><div class="pinned-container-wrapper-utils pinned-requests-wrapper-utils"><button class="btn-icon pinned-container-close pinned-requests-close"><span class="tgico button-icon"></span></button></div></div></div><div class="pinned-actions pinned-container hide"><div class="pinned-container-wrapper pinned-actions-wrapper rp"><div class="pinned-container-wrapper-utils pinned-actions-wrapper-utils"><button class="btn-icon pinned-container-close pinned-actions-close"><span class="tgico button-icon"></span></button></div></div></div></div><div class="bubbles scrolled-down has-groups has-sticky-dates"><div class="scrollable scrollable-y"><div class="bubbles-inner has-rights"><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">June 23</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">June 23</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="5645" data-peer-id="6086485359" data-timestamp="1687488400" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Pelajari materi 12-13 manajemen bisnis untuk Minggu depan<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:46 AM</span><div class="time-inner" title="23 June 2023, 09:46:40"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:46 AM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div><div class="bubbles-group"><div data-mid="5647" data-peer-id="6086485359" data-timestamp="1687488946" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Jgn lp membawa kertas A4 d potong 4 + pena<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:55 AM</span><div class="time-inner" title="23 June 2023, 09:55:46"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:55 AM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">June 27</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">June 27</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="5769" data-peer-id="6086485359" data-timestamp="1687854798" class="bubble hide-name is-out can-have-tail is-read is-group-first"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">1. She <em>____</em> her cat everyday. 
* 
2 poin 
to feed 
feed 
feeds 
feeding 
2. I <em>____</em> always <em>____</em> to the dentist. 
* 
2 poin 
do not, go 
does not, go 
do not, went 
does not, went 
3. They <em>__</em> (finish) building the house in next six months. 
* 
2 poin 
finished 
finishes 
are finishing 
will finish 
4.  Jaani <em>___</em> (dance) on the stage tomorrow. 
* 
2 poin 
dancing 
will dance 
dance 
is dancing 
5. Joan thinks the Conservatives <em>___</em> (win) the next selection. 
* 
2 poin 
will win 
win 
wins 
is winning 
6. A : I'm moving house tomorrow.      B : I <em>____</em> (come) and help you. 
* 
2 poin 
come 
comes 
will come 
am coming 
7. The flowers <em>____</em> (be, normally) watered by Grandma. 
* 
2 poin 
are normally 
normally are 
normally is 
is normally 
8. I <em>____</em> (not, know) what you mean. 
* 
2 poin 
doesn't know 
not know 
don't know 
am not know 
9. Ashley <em>___</em> (stay) at home tonight. 
* 
2 poin 
stays 
stayed 
will stay 
is staying 
10. She thinks Helen <em>____</em> always lucky. 
* 
2 poin 
are 
is 
am 
be 
11. It <em>___</em> (be) very hot this summer. 
* 
2 poin 
is 
will be 
being 
is being 
12. Rain <em>____</em> from the cloud. 
* 
2 poin 
falls 
fall 
falling 
is fall 
13. Every twelve months, the earth <em>____</em> the sun. 
* 
2 poin 
circle 
circles 
is circle 
circle is 
14. Denny and Charlie <em>____</em> their father on Sunday. 
* 
2 poin 
phons 
phones 
phone 
is phone 
15.  People <em>_______</em> on the moon in the future. 
* 
2 poin 
living 
will live 
are live 
be live 
16.  I <em>___</em> the book after I read it. 
* 
2 poin 
will return 
am return 
be return 
return 
17.  Tom <em>_________</em> at eight tomorrow morning. 
* 
2 poin 
will arrives 
arrives 
will going to arrive 
will arrive 
18. His students <em>____</em> (not, speak) German in class. 
* 
2 poin 
don't speak 
doesn't speak 
not speak 
aren't speak 
19. My mother usually <em>____</em> a nap after lunch. 
* 
2 poin 
takes 
is take 
take 
taking 
20. My parents <em>____</em> excuses when I feel like going to the cinema. 
* 
2 poin 
make always 
always make 
makes always 
always makes 
21. They <em>____</em> a letter for me. 
* 
2 poin 
sent 
send 
sends 
sending 
22.  A: Can you give Ann a message for me?  
       B: Sure, Probably I <em>_____________</em> him at the meeting this evening. 
* 
2 poin 
saw 
am seeing 
will see 
see 
23.  I <em>______</em> my friends for dinner after work tomorrow. 
* 
2 poin 
am meeting 
will meet 
meet 
be meet 
24.  If it doesn't rain tomorrow morning, we <em>_________</em>  our umbrellas. 
* 
2 poin 
won't take 
don't take 
aren't taking 
not take 
25. Q: When do you do your homework?  
      A: <em>____________</em> 
* 
2 poin 
I does do my homework at 6 p.m. 
I am do my homework at 6 p.m. 
I do my homework at 6 p.m. 
I doing my homework at 6 p.m. 
26. I <em>__</em> rarely at home on Saturday and Sunday. 
* 
2 poin 
is 
are 
be 
am 
27. They <em>__</em> some fruits for my family. 
* 
2 poin 
are buy 
buys 
buy 
is buy 
28. Selena and Aiko <em>__</em> in the living room right now. 
* 
2 poin 
are 
am 
is 
be 
29.  <em>__</em> your folks <em>_</em> before Tuesday? 
* 
2 poin 
Are, leaving 
Are, leave 
Do, leave 
Will, leave 
30.  I <em>__</em> you move your things tomorrow.   
* 
2 poin 
will help 
helping 
have been helping 
had helped 
31.  You <em>____</em> my friends next week. 
* 
2 poin 
meet 
will meet 
will be meet 
are meeting 
32. He <em>__</em> a chocolate bread every morning. 
* 
2 poin 
is eat 
eats 
are eat 
eat 
33. You and I <em>___</em> classmate. 
* 
2 poin 
are not 
is not 
not 
am not 
34. We <em>__</em> lyric of the romantic song. 
* 
2 poin 
are make 
is make 
makes 
make 
35. The cars <em>___</em> expensive. 
* 
2 poin 
am not 
is not 
not 
are not 
36. We <em>____</em> pictures in a few minutes.  
* 
2 poin 
will taking 
are taken 
take 
will take 
37.  My mom <em>____</em> some cookies for us later.  
* 
2 poin 
might make 
makes 
is making 
will make 
38. I <em>____</em> some new clothes this week.    
* 
2 poin 
will get 
get 
am getting 
be get 
39.  The people in Brazil <em>_______</em> Spanish. They speak Portuguese. 
* 
2 poin 
don't speak 
not speak 
won't speak 
not to speak 
40.  They <em>_______</em> the<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span><div class="time-inner" title="27 June 2023, 15:33:18"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div><div data-mid="5770" data-peer-id="6086485359" data-timestamp="1687854798" class="bubble hide-name is-out can-have-tail is-read"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">bus to <em>work</em> at eight o'clock every day. 
* 
2 poin 
catches 
catch 
will catch<em> 
ar</em>e catchi<em>ng 
</em>41.  Every morning I <em>________</em> at seven o'clock. 
* 
2 poin 
get up 
gets up 
am getting up 
i<em>s </em>getting up 
42.  I <em>____</em> some new clothes this week.    
* 
2 poin 
will be getting 
get 
are getting 
will get 
43.  I <em>______</em> this book about the first moon landing by tomorrow, and then you can borrow it. 
* 
2 poin 
finish 
are finished <em>
wi</em>ll finish 
to finish 
44.   Olga hopes she <em>______</em> a space mission in the next few years. 
* 
2 poin 
will lead 
leads<em> 
is</em> leading 
is leads 
45.  In the morning she always showers and <em>________</em> her hair. 
*<em> 
2 </em>poin 
comb 
combs 
is combing 
will comb 
46.  When I have finished dressing, I put on my shoes and <em>________. 
</em>* 
2 poin 
go downstairs 
goes downstairs 
am going downstairs 
is going downstairs 
47.  In Januar<em>y, </em>it <em>_________</em> very often. 
* 
2 poin 
will snow 
is snowing 
snows 
be snow 
48. I'm sorry. I <em>____ he</em>re tomorrow. 
* 
2 poin 
are not be 
won't be 
not <em>be </em>
not being 
49. Two days later, I <em>____</em> to Dubai for a conference. 
* 
2 poin 
g<em>o 
g</em>oing 
will go 
be go 
50. What’s happened to your hair? Your mother ___ it. 
* 
2 poin 
likes 
<em>is l</em>iking 
be like 
won't like<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span><div class="time-inner" title="27 June 2023, 15:33:18"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div><div data-mid="5771" data-peer-id="6086485359" data-timestamp="1687854803" class="bubble hide-name is-out can-have-tail is-read"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">1. She <em>____</em> her cat everyday. 
* 
2 poin 
to feed 
feed 
feeds 
feeding 
2. I <em>____</em> always <em>____</em> to the dentist. 
* 
2 poin 
do not, go 
does not, go 
do not, went 
does not, went 
3. They <em>__</em> (finish) building the house in next six months. 
* 
2 poin 
finished 
finishes 
are finishing 
will finish 
4.  Jaani <em>___</em> (dance) on the stage tomorrow. 
* 
2 poin 
dancing 
will dance 
dance 
is dancing 
5. Joan thinks the Conservatives <em>___</em> (win) the next selection. 
* 
2 poin 
will win 
win 
wins 
is winning 
6. A : I'm moving house tomorrow.      B : I <em>____</em> (come) and help you. 
* 
2 poin 
come 
comes 
will come 
am coming 
7. The flowers <em>____</em> (be, normally) watered by Grandma. 
* 
2 poin 
are normally 
normally are 
normally is 
is normally 
8. I <em>____</em> (not, know) what you mean. 
* 
2 poin 
doesn't know 
not know 
don't know 
am not know 
9. Ashley <em>___</em> (stay) at home tonight. 
* 
2 poin 
stays 
stayed 
will stay 
is staying 
10. She thinks Helen <em>____</em> always lucky. 
* 
2 poin 
are 
is 
am 
be 
11. It <em>___</em> (be) very hot this summer. 
* 
2 poin 
is 
will be 
being 
is being 
12. Rain <em>____</em> from the cloud. 
* 
2 poin 
falls 
fall 
falling 
is fall 
13. Every twelve months, the earth <em>____</em> the sun. 
* 
2 poin 
circle 
circles 
is circle 
circle is 
14. Denny and Charlie <em>____</em> their father on Sunday. 
* 
2 poin 
phons 
phones 
phone 
is phone 
15.  People <em>_______</em> on the moon in the future. 
* 
2 poin 
living 
will live 
are live 
be live 
16.  I <em>___</em> the book after I read it. 
* 
2 poin 
will return 
am return 
be return 
return 
17.  Tom <em>_________</em> at eight tomorrow morning. 
* 
2 poin 
will arrives 
arrives 
will going to arrive 
will arrive 
18. His students <em>____</em> (not, speak) German in class. 
* 
2 poin 
don't speak 
doesn't speak 
not speak 
aren't speak 
19. My mother usually <em>____</em> a nap after lunch. 
* 
2 poin 
takes 
is take 
take 
taking 
20. My parents <em>____</em> excuses when I feel like going to the cinema. 
* 
2 poin 
make always 
always make 
makes always 
always makes 
21. They <em>____</em> a letter for me. 
* 
2 poin 
sent 
send 
sends 
sending 
22.  A: Can you give Ann a message for me?  
       B: Sure, Probably I <em>_____________</em> him at the meeting this evening. 
* 
2 poin 
saw 
am seeing 
will see 
see 
23.  I <em>______</em> my friends for dinner after work tomorrow. 
* 
2 poin 
am meeting 
will meet 
meet 
be meet 
24.  If it doesn't rain tomorrow morning, we <em>_________</em>  our umbrellas. 
* 
2 poin 
won't take 
don't take 
aren't taking 
not take 
25. Q: When do you do your homework?  
      A: <em>____________</em> 
* 
2 poin 
I does do my homework at 6 p.m. 
I am do my homework at 6 p.m. 
I do my homework at 6 p.m. 
I doing my homework at 6 p.m. 
26. I <em>__</em> rarely at home on Saturday and Sunday. 
* 
2 poin 
is 
are 
be 
am 
27. They <em>__</em> some fruits for my family. 
* 
2 poin 
are buy 
buys 
buy 
is buy 
28. Selena and Aiko <em>__</em> in the living room right now. 
* 
2 poin 
are 
am 
is 
be 
29.  <em>__</em> your folks <em>_</em> before Tuesday? 
* 
2 poin 
Are, leaving 
Are, leave 
Do, leave 
Will, leave 
30.  I <em>__</em> you move your things tomorrow.   
* 
2 poin 
will help 
helping 
have been helping 
had helped 
31.  You <em>____</em> my friends next week. 
* 
2 poin 
meet 
will meet 
will be meet 
are meeting 
32. He <em>__</em> a chocolate bread every morning. 
* 
2 poin 
is eat 
eats 
are eat 
eat 
33. You and I <em>___</em> classmate. 
* 
2 poin 
are not 
is not 
not 
am not 
34. We <em>__</em> lyric of the romantic song. 
* 
2 poin 
are make 
is make 
makes 
make 
35. The cars <em>___</em> expensive. 
* 
2 poin 
am not 
is not 
not 
are not 
36. We <em>____</em> pictures in a few minutes.  
* 
2 poin 
will taking 
are taken 
take 
will take 
37.  My mom <em>____</em> some cookies for us later.  
* 
2 poin 
might make 
makes 
is making 
will make 
38. I <em>____</em> some new clothes this week.    
* 
2 poin 
will get 
get 
am getting 
be get 
39.  The people in Brazil <em>_______</em> Spanish. They speak Portuguese. 
* 
2 poin 
don't speak 
not speak 
won't speak 
not to speak 
40.  They <em>_______</em> the<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span><div class="time-inner" title="27 June 2023, 15:33:23"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div><div data-mid="5772" data-peer-id="6086485359" data-timestamp="1687854803" class="bubble hide-name is-out can-have-tail is-read is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">bus to <em>work</em> at eight o'clock every day. 
* 
2 poin 
catches 
catch 
will catch<em> 
ar</em>e catchi<em>ng 
</em>41.  Every morning I <em>________</em> at seven o'clock. 
* 
2 poin 
get up 
gets up 
am getting up 
i<em>s </em>getting up 
42.  I <em>____</em> some new clothes this week.    
* 
2 poin 
will be getting 
get 
are getting 
will get 
43.  I <em>______</em> this book about the first moon landing by tomorrow, and then you can borrow it. 
* 
2 poin 
finish 
are finished <em>
wi</em>ll finish 
to finish 
44.   Olga hopes she <em>______</em> a space mission in the next few years. 
* 
2 poin 
will lead 
leads<em> 
is</em> leading 
is leads 
45.  In the morning she always showers and <em>________</em> her hair. 
*<em> 
2 </em>poin 
comb 
combs 
is combing 
will comb 
46.  When I have finished dressing, I put on my shoes and <em>________. 
</em>* 
2 poin 
go downstairs 
goes downstairs 
am going downstairs 
is going downstairs 
47.  In Januar<em>y, </em>it <em>_________</em> very often. 
* 
2 poin 
will snow 
is snowing 
snows 
be snow 
48. I'm sorry. I <em>____ he</em>re tomorrow. 
* 
2 poin 
are not be 
won't be 
not <em>be </em>
not being 
49. Two days later, I <em>____</em> to Dubai for a conference. 
* 
2 poin 
g<em>o 
g</em>oing 
will go 
be go 
50. What’s happened to your hair? Your mother ___ it. 
* 
2 poin 
likes 
<em>is l</em>iking 
be like 
won't like<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span><div class="time-inner" title="27 June 2023, 15:33:23"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">03:33 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">August 16</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">August 16</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="6413" data-peer-id="6086485359" data-timestamp="1692165279" class="bubble forwarded is-out can-have-tail is-read is-group-first"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Berikut kami lampirkan informasi penyusunan KRS ganjil 2023/2024 bagi mahasiswa/i Institut Teknologi dan Bisnis Palcomtech. Penyusunan KRS mulai tanggal 5 s.d 7 September 2023. Terima kasih

<a class="anchor-url" href="https://palcomtech.ac.id/informasi-penginputan-krs-online-semua-angkatan-institut-teknologi-dan-bisnis-palcomtech-periode-ganjil-2023-2024/" target="_blank" rel="noopener noreferrer">https://palcomtech.ac.id/informasi-penginputan-krs-online-semua-angkatan-institut-teknologi-dan-bisnis-palcomtech-periode-ganjil-2023-2024/</a><span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:54 PM</span><div class="time-inner" title="16 August 2023, 12:54:39
Original: 16 August 2023, 11:50:37"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:54 PM</span></div></span></div><div class="name floating-part"><span class="i18n">Forwarded from <span class="peer-title" dir="auto" data-peer-id="5477033722" data-with-premium-icon="0">Informasi Terkini Institut PalComTech</span></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div><div data-mid="6414" data-peer-id="6086485359" data-timestamp="1692165279" class="bubble forwarded is-out can-have-tail is-read is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto"><a class="anchor-url" href="https://palcomtech.ac.id/informasi-penginputan-krs-online-semua-angkatan-institut-teknologi-dan-bisnis-palcomtech-periode-ganjil-2023-2024/" target="_blank" rel="noopener noreferrer">https://palcomtech.ac.id/informasi-penginputan-krs-online-semua-angkatan-institut-teknologi-dan-bisnis-palcomtech-periode-ganjil-2023-2024/</a><span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:54 PM</span><div class="time-inner" title="16 August 2023, 12:54:39
Original: 16 August 2023, 11:52:03"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:54 PM</span></div></span></div><div class="name floating-part"><span class="i18n">Forwarded from <span class="peer-title" dir="auto" data-peer-id="262933863" data-with-premium-icon="0">Yayuk Ike Meilani</span></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 2</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 2</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="6621" data-peer-id="6086485359" data-timestamp="1696211988" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Hallo g<em>ood morning  sir and all of my friends here!</em>
Let me introduce myself…
<em>My full name is…</em>mutiara
Date of Birth..l was born in april 04,2004.
Gender...perempuan
<em>Contact Number&nbsp;</em>..082321385800
State of Residence...sumatra selatan
Address..Jln,tanjung api-api
Hobby...my hobby is<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">08:59 AM</span><div class="time-inner" title="2 October 2023, 08:59:48"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">08:59 AM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div><div class="bubbles-group"><div data-mid="6623" data-peer-id="6086485359" data-timestamp="1696212416" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Assalamualaikum wr.wb.

Hello good morning sir and all of my friends here
 myself. Yes my name is mutiara you can call me mutia , I'm 19 years old,
I live in Palembang 
I was born in banyuasin On 02 
April -2004
My religion is muslem,
I have a hobby , my hobby is voli the ,batminton
You can mention me in Instagram, my account Instagram is mutiaramuti306


Oke guy's, <em>This is the end of my self-introduction</em> myself, thank you wassalamualaikum wr wb<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:06 AM</span><div class="time-inner" title="2 October 2023, 09:06:56"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">09:06 AM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 3</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 3</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="6749" data-peer-id="6086485359" data-timestamp="1696317674" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">p<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">02:21 PM</span><div class="time-inner" title="3 October 2023, 14:21:14"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">02:21 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div><div class="bubbles-group"><div data-mid="6750" data-peer-id="6086485359" data-timestamp="1696317689" class="bubble hide-name is-in can-have-tail is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Ngpo yng<span class="time"><span class="i18n" dir="auto">02:21 PM</span><div class="time-inner" title="3 October 2023, 14:21:29"><span class="i18n" dir="auto">02:21 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div><div class="bubbles-group"><div data-mid="6751" data-peer-id="6086485359" data-timestamp="1696317696" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">paket aku habis<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">02:21 PM</span><div class="time-inner" title="3 October 2023, 14:21:36"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">02:21 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div><div class="bubbles-group"><div data-mid="6752" data-peer-id="6086485359" data-timestamp="1696317714" class="bubble hide-name is-in can-have-tail is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Belum blk<span class="time"><span class="i18n" dir="auto">02:21 PM</span><div class="time-inner" title="3 October 2023, 14:21:54"><span class="i18n" dir="auto">02:21 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date is-sticky"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 13</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 13</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="6958" data-peer-id="6086485359" data-timestamp="1697191781" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Norma dan kaidah dalam etika adalah prinsip-prinsip atau aturan moral yang mengatur perilaku dan interaksi manusia. Beberapa norma dan kaidah umum dalam etika meliputi:

Prinsip Autonomi: Menghormati otonomi individu, yaitu kemampuan seseorang untuk membuat keputusan berdasarkan nilai-nilai dan keyakinan mereka sendiri.

Prinsip Beneficence: Berbuat baik dan bertindak demi kesejahteraan orang lain.

Prinsip Non-Maleficence: Mencegah kerusakan atau penghancuran, yaitu tidak menyebabkan kerugian yang tidak diperlukan.

Prinsip Keadilan: Menyediakan perlakuan yang adil dan setara terhadap semua individu, tanpa diskriminasi.

Prinsip Kejujuran: Berbicara jujur dan jangan menipu atau menyesatkan.

Prinsip Tidak Merugikan Orang Lain: Tidak melakukan tindakan yang merugikan atau merugikan orang lain.

Prinsip Kesetiaan: Menepati komitmen dan janji yang telah dibuat.

Norma dan kaidah etika dapat bervariasi tergantung pada budaya, nilai, dan konteks tertentu. Etika sering digunakan untuk membimbing perilaku manusia dalam berbagai aspek kehidupan, termasuk di bidang medis, bisnis, hukum,<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">05:09 PM</span><div class="time-inner" title="13 October 2023, 17:09:41"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">05:09 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 16</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">October 16</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="7086" data-peer-id="6086485359" data-timestamp="1697426680" class="bubble hide-name is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto">Ini jwbn untuk klpok 1 kk<span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">10:24 AM</span><div class="time-inner" title="16 October 2023, 10:24:40"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">10:24 AM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">November 1</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n" dir="auto">November 1</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="8300" data-peer-id="6086485359" data-timestamp="1698833047" class="bubble hidden-profile forwarded is-out can-have-tail is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container document-message"><div class="document-container" data-mid="8300" data-peer-id="6086485359"><div class="document-wrapper"><div class="document ext-pptx" data-doc-id="6118446372195667413"><div class="document-ico"><span class="document-ico-text">pptx</span><div class="preloader-container manual is-visible">
    <div class="you-spin-me-round">
    <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="27 27 54 54">
    <circle class="preloader-path-new" cx="54" cy="54" r="24" fill="none" stroke-miterlimit="10"></circle>
    </svg>
    </div>
      <svg xmlns="http://www.w3.org/2000/svg" class="preloader-close" viewBox="0 0 24 24">
        <g fill="none" fill-rule="evenodd">
          <polygon points="0 0 24 0 24 24 0 24"></polygon>
          <path fill="#000" fill-rule="nonzero" d="M5.20970461,5.38710056 L5.29289322,5.29289322 C5.65337718,4.93240926 6.22060824,4.90467972 6.61289944,5.20970461 L6.70710678,5.29289322 L12,10.585 L17.2928932,5.29289322 C17.6834175,4.90236893 18.3165825,4.90236893 18.7071068,5.29289322 C19.0976311,5.68341751 19.0976311,6.31658249 18.7071068,6.70710678 L13.415,12 L18.7071068,17.2928932 C19.0675907,17.6533772 19.0953203,18.2206082 18.7902954,18.6128994 L18.7071068,18.7071068 C18.3466228,19.0675907 17.7793918,19.0953203 17.3871006,18.7902954 L17.2928932,18.7071068 L12,13.415 L6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L10.585,12 L5.29289322,6.70710678 C4.93240926,6.34662282 4.90467972,5.77939176 5.20970461,5.38710056 L5.29289322,5.29289322 L5.20970461,5.38710056 Z"></path>
        </g>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" class="preloader-download" viewBox="0 0 24 24">
        <g fill="none" fill-rule="evenodd">
          <polygon points="0 0 24 0 24 24 0 24"></polygon>
          <path fill="#000" fill-rule="nonzero" d="M5,19 L19,19 C19.5522847,19 20,19.4477153 20,20 C20,20.5128358 19.6139598,20.9355072 19.1166211,20.9932723 L19,21 L5,21 C4.44771525,21 4,20.5522847 4,20 C4,19.4871642 4.38604019,19.0644928 4.88337887,19.0067277 L5,19 L19,19 L5,19 Z M11.8833789,3.00672773 L12,3 C12.5128358,3 12.9355072,3.38604019 12.9932723,3.88337887 L13,4 L13,13.585 L16.2928932,10.2928932 C16.6533772,9.93240926 17.2206082,9.90467972 17.6128994,10.2097046 L17.7071068,10.2928932 C18.0675907,10.6533772 18.0953203,11.2206082 17.7902954,11.6128994 L17.7071068,11.7071068 L12.7071068,16.7071068 C12.3466228,17.0675907 11.7793918,17.0953203 11.3871006,16.7902954 L11.2928932,16.7071068 L6.29289322,11.7071068 C5.90236893,11.3165825 5.90236893,10.6834175 6.29289322,10.2928932 C6.65337718,9.93240926 7.22060824,9.90467972 7.61289944,10.2097046 L7.70710678,10.2928932 L11,13.585 L11,4 C11,3.48716416 11.3860402,3.06449284 11.8833789,3.00672773 L12,3 L11.8833789,3.00672773 Z"></path>
        </g>
      </svg></div></div>
  
  <div class="document-name"><middle-ellipsis-element data-font-weight="500" data-font-size="16" data-size-type="documentName" title="Pemrograman Web Dasar Materi full.pptx">Pemrograman Web…teri full.pptx</middle-ellipsis-element></div>
  <div class="document-size"><span><span class="i18n">7.9 MB</span> · <span style="visibility: hidden;"> / <span class="i18n">7.9 MB</span></span></span></div>
  <span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">05:04 PM</span><div class="time-inner" title="1 November 2023, 17:04:07
Original: 5 October 2023, 11:15:12"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">05:04 PM</span></div></span></div></div></div></div><div class="name floating-part"><span class="i18n">Forwarded from <span class="peer-title" dir="auto" data-peer-id="0">Andika Widyanto</span></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section><section class="bubbles-date-group"><div class="bubble service is-date"><div class="bubble-content"><div class="service-msg"><span class="i18n">Today</span></div></div></div><div class="bubble service is-date is-fake"><div class="bubble-content"><div class="service-msg"><span class="i18n">Today</span></div></div></div><div class="sticky_sentinel sticky_sentinel--top"></div><div class="bubbles-group"><div data-mid="8301" data-peer-id="6086485359" data-timestamp="1698901363" class="bubble is-message-empty hide-name photo is-out is-read is-group-first is-group-last"><div class="bubble-content-wrapper" style=""><div class="bubble-content"><div class="message spoilers-container" dir="auto"><span class="time"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:02 PM</span><div class="time-inner" title="2 November 2023, 12:02:43"><span class="tgico time-sending-status"></span><span class="i18n" dir="auto">12:02 PM</span></div></span></div><div class="attachment media-container" style="width: 288px; height: 340px;"><img class="media-photo" decoding="async" src="blob:https://web.telegram.org/a918838f-a550-44d4-a4a8-ddc5360d18e3"></div></div></div></div></div><div class="bubbles-group"><div data-mid="8302" data-peer-id="6086485359" data-timestamp="1698901952" class="bubble hide-name is-in can-have-tail is-group-first is-group-last"><div class="bubble-content-wrapper"><div class="bubble-content"><div class="message spoilers-container" dir="auto">Ngpi ini<span class="time"><span class="i18n" dir="auto">12:12 PM</span><div class="time-inner" title="2 November 2023, 12:12:32"><span class="i18n" dir="auto">12:12 PM</span></div></span></div><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg></div></div></div></div></section></div></div></div><div class="chat-input chat-input-main"><div class="chat-input-container chat-input-main-container"><div class="rows-wrapper-wrapper"><div class="rows-wrapper chat-input-wrapper chat-input-main-wrapper chat-rows-wrapper"><svg viewBox="0 0 11 20" width="11" height="20" class="bubble-tail"><use href="#message-tail-filled"></use></svg><div class="autocomplete-helper z-depth-1 autocomplete-peer-helper bot-commands"></div><div class="reply-wrapper rows-wrapper-row"><button class="btn-icon rp" tabindex="-1"><div class="c-ripple"></div></button><button class="btn-icon reply-cancel" tabindex="-1"><span class="tgico button-icon"></span></button><div class="btn-menu"><form><div class="btn-menu-item rp-overflow"><span class="i18n btn-menu-item-text">Chat.Alert.Forward.Action.Show1</span><label class="checkbox-field checkbox-without-caption"><input class="checkbox-field-input" type="radio" name="author" value="1"><div class="checkbox-box"><div class="checkbox-box-border"></div><div class="checkbox-box-background"></div><svg class="checkbox-box-check" viewBox="0 0 24 24"><use href="#check" x="-1"></use></svg></div></label></div><div class="btn-menu-item rp-overflow"><span class="i18n btn-menu-item-text">Chat.Alert.Forward.Action.Hide1</span><label class="checkbox-field checkbox-without-caption"><input class="checkbox-field-input" type="radio" name="author" value="0"><div class="checkbox-box"><div class="checkbox-box-border"></div><div class="checkbox-box-background"></div><svg class="checkbox-box-check" viewBox="0 0 24 24"><use href="#check" x="-1"></use></svg></div></label></div><hr></form><form><div class="btn-menu-item rp-overflow"><span class="i18n btn-menu-item-text">Chat.Alert.Forward.Action.ShowCaption</span><label class="checkbox-field checkbox-without-caption"><input class="checkbox-field-input" type="radio" name="caption" value="1"><div class="checkbox-box"><div class="checkbox-box-border"></div><div class="checkbox-box-background"></div><svg class="checkbox-box-check" viewBox="0 0 24 24"><use href="#check" x="-1"></use></svg></div></label></div><div class="btn-menu-item rp-overflow"><span class="i18n btn-menu-item-text">Chat.Alert.Forward.Action.HideCaption</span><label class="checkbox-field checkbox-without-caption"><input class="checkbox-field-input" type="radio" name="caption" value="0"><div class="checkbox-box"><div class="checkbox-box-border"></div><div class="checkbox-box-background"></div><svg class="checkbox-box-check" viewBox="0 0 24 24"><use href="#check" x="-1"></use></svg></div></label></div><hr></form><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Forward to Another Chat</span></div></div></div><div class="autocomplete-helper z-depth-1 stickers-helper"></div><div class="autocomplete-helper z-depth-1 emoji-helper"></div><div class="autocomplete-helper z-depth-1 autocomplete-peer-helper commands-helper"></div><div class="autocomplete-helper z-depth-1 autocomplete-peer-helper mentions-helper"></div><div class="autocomplete-helper z-depth-1 inline-helper"></div><div class="new-message-wrapper rows-wrapper-row" data-offset="commands"><button class="btn-icon toggle-emoticons" tabindex="-1"><span class="tgico button-icon"></span></button><div class="input-message-container"><div class="input-message-input is-empty i18n scrollable scrollable-y no-scrollbar" contenteditable="true" dir="auto" data-placeholder="Message" tabindex="-1" data-peer-id="6086485359" style="transition-duration: 0ms; height: 37px;"></div><div contenteditable="true" tabindex="-1" class="input-message-input is-empty i18n scrollable scrollable-y no-scrollbar input-field-input-fake"></div></div><button class="btn-icon btn-scheduled float hide show" tabindex="-1"><span class="tgico button-icon"></span></button><button class="btn-icon toggle-reply-markup float hide show" tabindex="-1"><span class="tgico button-icon"></span></button><div class="btn-icon btn-menu-toggle attach-file"><span class="tgico"></span></div><div class="record-time"></div><input type="file" multiple="" style="display: none;"></div></div></div><div class="fake-wrapper fake-rows-wrapper"></div><div class="fake-wrapper fake-selection-wrapper"></div><button class="btn-circle btn-corner z-depth-1 bubbles-corner-button chat-secondary-button bubbles-go-down rp" tabindex="-1"><div class="c-ripple"></div><span class="tgico button-icon"></span><span class="badge badge-24 badge-primary badge-gray is-badge-empty"></span></button><div class="chat-input-control chat-input-wrapper"><button class="btn-primary btn-transparent text-bold chat-input-control-button rp hide"><div class="c-ripple"></div><span class="i18n">START</span></button><button class="btn-primary btn-transparent text-bold chat-input-control-button rp"><div class="c-ripple"></div><span class="i18n">Unblock</span></button><div class="reply-in-topic-overlay hide"><span class="i18n">Reply to message in topics</span></div><button class="btn-primary btn-transparent text-bold chat-input-control-button rp hide"><span class="i18n">Unpin All Messages</span></button></div><button class="btn-circle btn-corner z-depth-1 bubbles-corner-button chat-secondary-button bubbles-go-mention rp" tabindex="-1"><div class="c-ripple"></div><span class="tgico button-icon"></span><span class="badge badge-24 badge-primary is-badge-empty"></span></button><button class="btn-icon btn-circle btn-record-cancel chat-input-secondary-button chat-secondary-button rp" tabindex="-1"><div class="c-ripple"></div><span class="tgico button-icon"></span></button><div class="btn-send-container"><div class="record-ripple"></div><button class="btn-icon rp btn-circle btn-send animated-button-icon record" tabindex="-1"><div class="c-ripple"></div><span class="tgico animated-button-icon-icon btn-send-icon-send"></span><span class="tgico animated-button-icon-icon btn-send-icon-schedule"></span><span class="tgico animated-button-icon-icon btn-send-icon-edit"></span><span class="tgico animated-button-icon-icon btn-send-icon-record"></span><span class="tgico animated-button-icon-icon btn-send-icon-forward"></span></button><div class="btn-menu menu-send top-left"><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Send Without Sound</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Schedule Message</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Set a Reminder</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Send When Online</span></div></div></div></div></div><div class="btn-menu contextmenu has-items-wrapper center-right was-open" id="bubble-contextmenu" style="min-width: 194px; left: 1341px; top: 727px;"><div class="btn-menu-reactions-container btn-menu-reactions-container-horizontal btn-menu-transition is-visible"><div class="btn-menu-reactions-bubble btn-menu-reactions-bubble-big"></div><div class="btn-menu-reactions"><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="5051049885634134593" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="5051019936827179425" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="5051235660149555648" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="5053080370078024140" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="4985723613450601114" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="4987918217184805352" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="5048978706375115415" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="4976968996927570535" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="4985782815279809277" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="4985699063417537034" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="5172487685041816252" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="5172611693632553503" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><div class="btn-menu-reactions-reaction"><div class="btn-menu-reactions-reaction-scale"><div class="btn-menu-reactions-reaction-appear media-sticker-wrapper hide" data-doc-id="5068882272019546548" data-sticker-play="1" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div><div class="btn-menu-reactions-reaction-select media-sticker-wrapper" data-doc-id="5069061131637621524" data-sticker-play="0" data-sticker-loop="0"><canvas class="rlottie" width="28" height="28"></canvas></div></div></div><button class="btn-icon btn-menu-reactions-more"><span class="tgico button-icon"></span></button></div></div><div class="btn-menu-items btn-menu-transition"><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Reply</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Edit</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Pin</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Download</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Forward</span></div><div class="btn-menu-item rp-overflow"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Select</span></div><div class="btn-menu-item rp-overflow danger"><span class="tgico btn-menu-item-icon"></span><span class="i18n btn-menu-item-text">Delete</span></div></div></div></div></div></div>
        <div class="tabs-tab sidebar sidebar-right main-column" id="column-right">
          <div class="sidebar-content sidebar-slider tabs-container" data-animation="navigation"><div class="tabs-tab sidebar-slider-item scrolled-top scrolled-bottom scrollable-y-bordered shared-media-container profile-container"><div class="sidebar-header"><button class="btn-icon sidebar-close-button"><div class="animated-close-icon"></div></button><div class="transition slide-fade" data-animation="slide-fade"><div class="transition-item active"><div class="sidebar-header__title"><span class="i18n">Profile</span></div><button class="btn-icon rp"><div class="c-ripple"></div><span class="tgico button-icon"></span></button></div><div class="transition-item"><div class="sidebar-header__title"><span class="i18n">Members</span></div></div><div class="transition-item"><div class="sidebar-header__title"><span class="i18n">Public Stories</span></div></div><div class="transition-item"><div class="sidebar-header__title"><span class="i18n">Shared Media</span></div></div><div class="transition-item"><div class="sidebar-header__title"><span class="i18n">Groups</span></div></div></div></div><div class="sidebar-content"><div class="scrollable scrollable-y no-parallax"><div class="profile-content"><div class="sidebar-left-section-container"><div class="sidebar-left-section no-delimiter"><div class="sidebar-left-section-content"><div class="avatar avatar-like avatar-120 profile-avatar" data-peer-id="6086485359" data-color="green">S</div><div class="profile-name"><span class="peer-title" dir="auto" data-peer-id="6086485359" data-dialog="1" data-with-icons="1" data-thread-id="0">Sayang<img src="./FT MUTIA_files/1f412.png" class="emoji emoji-image" alt="🐒"><img src="./FT MUTIA_files/1f90d.png" class="emoji emoji-image" alt="🤍"></span></div><div class="profile-subtitle"><span class="i18n">last seen recently</span></div><div class="row row-with-icon row-with-padding row-clickable hover-effect rp" style=""><div class="c-ripple"></div><div class="row-subtitle" dir="auto"><span class="i18n">Phone</span></div><div class="row-title" dir="auto">+62 815 41150577</div><span class="tgico row-icon"></span></div><div class="row row-with-icon row-with-padding row-clickable hover-effect rp" style="display: none;"><div class="c-ripple"></div><div class="row-subtitle" dir="auto"><span class="i18n">Username</span></div><div class="row-title" dir="auto"></div><span class="tgico row-icon"></span></div><div class="row row-with-icon row-with-padding" style="display: none;"><div class="row-subtitle" dir="auto"><span class="i18n">Location</span></div><div class="row-title" dir="auto"> </div><span class="tgico row-icon"></span></div><div class="row row-with-icon row-with-padding row-clickable hover-effect rp" style="display: none;"><div class="c-ripple"></div><div class="row-subtitle" dir="auto"><span class="i18n">Bio</span></div><div class="row-title pre-wrap" dir="auto"></div><span class="tgico row-icon"></span></div><div class="row row-with-icon row-with-padding row-clickable hover-effect rp" style="display: none;"><div class="c-ripple"></div><div class="row-subtitle" dir="auto"><span class="i18n">Link</span></div><div class="row-title" dir="auto"> </div><span class="tgico row-icon"></span></div><label class="row no-subtitle row-with-toggle row-with-icon row-with-padding row-clickable hover-effect rp"><div class="c-ripple"></div><div class="row-row row-title-row"><div class="row-title" dir="auto"><span class="i18n">Notifications</span></div><div class="row-title row-title-right"><label class="checkbox-field checkbox-without-caption checkbox-field-toggle disable-hover"><input class="checkbox-field-input" type="checkbox"><div class="checkbox-toggle"><div class="checkbox-toggle-circle"></div></div></label></div></div><span class="tgico row-icon"></span></label></div></div></div><div class="search-super"><div class="search-super-tabs-scrollable menu-horizontal-scrollable sticky"><div class="scrollable scrollable-x search-super-nav-scrollable"><nav class="search-super-tabs menu-horizontal-div"><div class="menu-horizontal-div-item rp hide"><span class="menu-horizontal-div-item-span"><span class="i18n">Stories</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp hide"><span class="menu-horizontal-div-item-span"><span class="i18n">Members</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp active"><span class="menu-horizontal-div-item-span"><span class="i18n">Media</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Files</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp"><span class="menu-horizontal-div-item-span"><span class="i18n">Links</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp hide"><span class="menu-horizontal-div-item-span"><span class="i18n">Music</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp hide"><span class="menu-horizontal-div-item-span"><span class="i18n">Voice</span><i></i></span><div class="c-ripple"></div></div><div class="menu-horizontal-div-item rp hide"><span class="menu-horizontal-div-item-span"><span class="i18n">Groups</span><i></i></span><div class="c-ripple"></div></div></nav></div></div><div class="search-super-tabs-container tabs-container" data-animation="tabs"><div class="search-super-tab-container search-super-container-stories tabs-tab"><div class="search-super-content-container search-super-content-stories"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-members tabs-tab"><div class="search-super-content-container search-super-content-members"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-media tabs-tab active"><div class="search-super-content-container search-super-content-media"><div class="search-super-month"><div class="search-super-month-name"><span class="i18n" dir="auto">November</span></div><div class="search-super-month-items"><div class="grid-item media-container search-super-item" data-mid="8301" data-peer-id="6086485359"><img decoding="async" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDACgcHiMeGSgjISMtKygwPGRBPDc3PHtYXUlkkYCZlo+AjIqgtObDoKrarYqMyP/L2u71////m8H////6/+b9//j/2wBDASstLTw1PHZBQXb4pYyl+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj/wAARCAAoACIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwC35yhsVG0paTbzxzUThvtGR93PPFJGWEkgfoP5VDbZVkTpKN7IM5Xt7VJ5mOe2KroFaYuOecDH0pRu+0Hk7eeO1GqDQt5opaK0IKjD5+veoJ5SGYcfNxmpCw809+ar3GCM9xWKepqkTW8x8zYR15zipgf35Hsar2xJ+dup4BNWhjqOtNvUJLXQeZQD/wDWop5jGaK1MjPikRyQzqB+NRNtMn31cegyKKKiyNSQhww9AOPzpy7wzbSML70UUWQ47EwaYj/WH8qKKKq4uVH/2Q==" class="thumbnail media-photo grid-item-media"></div></div></div></div></div><div class="search-super-tab-container search-super-container-files tabs-tab"><div class="search-super-content-container search-super-content-files"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-links tabs-tab"><div class="search-super-content-container search-super-content-links"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-music tabs-tab"><div class="search-super-content-container search-super-content-music"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-voice tabs-tab"><div class="search-super-content-container search-super-content-voice"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div><div class="search-super-tab-container search-super-container-groups tabs-tab"><div class="search-super-content-container search-super-content-groups"></div><div class="preloader">
  <svg xmlns="http://www.w3.org/2000/svg" class="preloader-circular" viewBox="25 25 50 50">
  <circle class="preloader-path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle>
  </svg></div></div></div></div></div></div><button class="btn-circle btn-corner z-depth-1 rp is-hidden" tabindex="-1"><div class="c-ripple"></div><span class="tgico button-icon"></span></button></div></div></div>
        </div>
      </div>
    </div>
    <div id="stories-viewer"></div>
    
  

<div style="display: none;"></div><div id="notify-sound"></div><div class="emoji-animation-container"></div></body></html>